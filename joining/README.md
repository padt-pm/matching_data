#USAGE
## csv joiner
concatenates files <data_file1>, <data_file2>, <data_file3>, ..., <data_fileN> and saves as <save_path>.csv 
```text
python3 join_csv.py <data_file1> <data_file2> <data_file3> ... <data_fileN> <save_path> 
```