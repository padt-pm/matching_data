import pandas as pd
import matplotlib.pyplot as plt
from wordcloud import WordCloud


def plot_stars(d, save_path):
    lists = sorted(d.items())
    x, y = zip(*lists)
    plt.bar(x, y)
    plt.xlabel('Stars')
    plt.ylabel('Count')
    plt.title('Stars distribution', fontsize=20)
    plt.autoscale()
    plt.savefig(save_path)
    plt.show()


def generate_word_map_for_reviews_with_x_stars(reviews_series, stars_count):
    str_list = [rev['Tekst'] for p in reviews_series for rev in p if rev['Ocena'] == stars_count]
    text = ' '.join(str_list).lower()
    wordcloud = WordCloud(width=600, height=300, max_font_size=50, max_words=50, background_color="white")\
        .generate(text)
    plt.figure()
    plt.imshow(wordcloud, interpolation="bilinear")
    plt.axis("off")
    plt.title(f'{stars_count} stars', fontsize=20)
    plt.savefig(f'images/wm_{stars_count}_stars.png')
    plt.show()


print('READING DATA')
allegro_df = pd.read_json('../allegro_frisco_LC_lublin.json')
print(list(allegro_df.keys()))
print(len(allegro_df))

stars_distribution = dict()
for p in allegro_df['Opinie']:
    for rev in p:
        o = rev['Ocena']
        if o not in stars_distribution:
            stars_distribution[o] = 0
        else:
            stars_distribution[o] += 1


plot_stars(stars_distribution, 'images/stars_dist.svg')


for s in stars_distribution.keys():
    generate_word_map_for_reviews_with_x_stars(allegro_df['Opinie'], s)
