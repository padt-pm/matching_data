#USAGE
## json normalizer
Extracts fields from <data_file>, perform key mapping according to <mapping_file> and saves as <save_path>.csv 
```text
python3 json_normalization.py <mapping_file> <data_file> <save_path> 
```

Mapping json format:
```json
{
  "key_mappings": {
    "key_in_file1": "new_key1",
    "key_in_file2": "new_key2"
  },
  "additional_fields": {
    "field_name1": "value", 
    "field_name2": "value"  
  }
}
```