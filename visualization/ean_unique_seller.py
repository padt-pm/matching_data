import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv('../joining/all_joined.csv', index_col=0)
grouped = df\
    .groupby(['EAN', 'seller']).size().reset_index(name='entries_seller')\
    .groupby('EAN').size().reset_index(name='ean_count')\
    .groupby('ean_count').size().reset_index(name='count')



sns.set_theme(style="whitegrid")
ax = sns.barplot(data=grouped, x='ean_count', y='count')
plt.ylabel('EAN count')
plt.xlabel('Number of unique sellers per EAN')
plt.savefig('images/eans_sellers_count.png', dpi=300)
plt.show()
