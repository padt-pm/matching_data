import pandas as pd
import sys

num_of_files = len(sys.argv)
files = []
for i in range(1, num_of_files - 1):
    files.append(sys.argv[i])
    print(sys.argv[i])

save_path = sys.argv[num_of_files - 1]

if not save_path.endswith('.csv'):
    save_path += '.csv'

dfs = []
for f in files:
    dfs.append(pd.read_csv(f, dtype=str, index_col=0))

joined = pd.concat(dfs, sort=True, ignore_index=True)

joined.to_csv(save_path)
