import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

number_of_most_used_attributes = 15
number_of_least_used_attributes = 15


def save_plot(data, path_to_save):
    plt.figure(figsize=(16, 12))
    sns.set_theme(style="whitegrid")
    ax = sns.barplot(data=data, y='attribute', x='percentage')

    plt.ylabel('Attribute name')
    plt.xlabel('Percentage of offers with given attribute')
    plt.savefig(path_to_save, dpi=300)
    plt.show()


for path, common_name in zip(
        ['../allegro_frisco_LC_lublin.json', '../frisco.json', '../leclerc_drive_lublin.json'],
        ['allegro_frisco_LC_lublin', 'frisco', 'leclerc_drive_lublin']
):
    print('READING DATA')
    print(common_name)
    if path.endswith('.json'):
        df = pd.read_json(path)
    elif path.endswith('.csv'):
        df = pd.read_csv(path)

    percentage_attribute_usage = (df.notna().mean().multiply(100)).sort_values(ascending=False)

    # top_x = percentage_attribute_usage.nlargest(number_of_most_used_attributes)
    # top_x = pd.DataFrame({'attribute': top_x.index, 'percentage': top_x.values})
    # bottom_x = percentage_attribute_usage.nsmallest(number_of_most_used_attributes)
    # bottom_x = pd.DataFrame({'attribute': bottom_x.index, 'percentage': bottom_x.values})

    # save_plot(top_x, f'images/top_{number_of_most_used_attributes}_used_attributes.png')
    # save_plot(bottom_x, f'images/bottom_{number_of_most_used_attributes}_used_attributes.png')

    percentage_df = pd.DataFrame({'attribute': percentage_attribute_usage.index,
                                  'percentage': percentage_attribute_usage.values})

    percentage_df.to_csv(f"fulfillment/{common_name}.csv", index=False)

    attributes_to_delete = percentage_df.loc[percentage_df['percentage'] < 5]
    attributes_to_delete.to_csv(f"attributes_to_drop/{common_name}.csv", index=False)
