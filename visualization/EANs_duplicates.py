import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df = pd.read_csv('../joining/all_joined.csv', index_col=0)
ean_sellers = df.groupby(['EAN', 'seller']).size().reset_index(name='entries_seller')

grouped = ean_sellers.groupby('entries_seller').size().reset_index(name='count')


sns.set_theme(style="whitegrid")
ax = sns.barplot(data=grouped, x='entries_seller', y='count')
plt.xlabel('Number of EAN entries per seller')
plt.savefig('images/eans_duplicates.png', dpi=300)
plt.show()
