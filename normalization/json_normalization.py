import pandas as pd
import json
import sys


class JSON_normalizer:
    def __init__(self, mapping_file):
        with open(mapping_file) as f:
            self.mapping = json.load(f)
        self.features = list(self.mapping['key_mappings'].values()) + list(self.mapping['additional_fields'].keys())

    def map_json_to_dataframe(self, data_file):
        with open(data_file) as f:
            data = json.load(f)
        rows = []
        for item in data:
            row = {}
            for k, v in self.mapping['key_mappings'].items():
                if k in item.keys():
                    row[v] = item[k]
            for k, v in self.mapping['additional_fields'].items():
                row[k] = v
            rows.append(row)
        return pd.DataFrame(rows, columns=self.features)


normalizer = JSON_normalizer(sys.argv[1])
df = normalizer.map_json_to_dataframe(sys.argv[2])
save_path = sys.argv[3]
if not save_path.endswith('.csv'):
    save_path += '.csv'
df.to_csv(save_path)

